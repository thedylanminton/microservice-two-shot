from django.shortcuts import render
from common.json import ModelEncoder
from .models import Hats, LocationVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
    ]


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


class HatListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "style_name",
        "fabric",
        "color",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

        try:
            id = content["location"]
            href = f'/api/locations/{id}/'
            location = LocationVO.objects.get(import_href=href)
            content["location"] = location

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location ID hats"},
                status=400,
            )
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, id):
    if request.method == "GET":
        hats = Hats.objects.get(id=id)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationVO.objects.get(id=content["location"])
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location ID"},
                status=400
            )
        Hats.objects.filter(id=id).update(**content)
        hats = Hats.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=HatsDetailEncoder,
            safe=False,
        )
