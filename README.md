# Wardrobify

Team:

* Andrew C. - Hats Microservice
* Dylan M. - Shoes Microservice

## Design

## Shoes microservice

The Shoe model tracks the manufacturer, model name, color, a URL for a picture of the model, and the bin the shoe instance is stored in. The Shoe app makes an API call using the poller.py module to populate the database with BinVOs. When created, the bin attribute of each shoe contains all the data of the BinVO that was retrieved from the wardrobe microservice API via the poller.

This shoe model has two API views: one that retrieves a list of shoes, and another to retrieve a specific shoes's data. These view functions are used to create shoes and delete shoes respectively.

To display the data there are three React components: ShoeList, ShoeDetails, and ShoeForm. ShoeList displays all shoes with their manufacturer and model name. Shoe details gets a specific shoe through its ID and displays the former information, as well as the color and the image associated with the shoe and contains a button to delete that shoe. Finally, ShoeForm is linked to in the ShoeList component and allows the user to create a shoe.

## Hats microservice

Created a hat and location model. Created a list & detail views with urls. Used a poller to poll location data from Wardrobe API. Website show a list of hats with a button to delete, and a formm to create new hats
