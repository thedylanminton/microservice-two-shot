import { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import ShoeDetails from './ShoeDetails';
import HatList from './HatList';
import HatForm from './HatForm';

function App(props) {
  const [ shoes, setShoes ] = useState([]);

  async function getShoes() {
    const response = await fetch('http://localhost:8080/api/shoes/');
    if (response.ok) {
      const { shoes } = await response.json();
      setShoes(shoes);
    } else {
      console.error('An error occurred fetching the data');
    }
  }

  useEffect(() => {
    getShoes();
  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="" element={<ShoesList shoes={shoes} />} />
            <Route path="new" element={<ShoeForm getShoes={getShoes} />} />
            <Route path=":id" element={<ShoeDetails getShoes={getShoes} />} />
          </Route>
          <Route path="hats">
            <Route path="" element={<HatList hats={props.hats} />} />
            <Route path="new" element={<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
