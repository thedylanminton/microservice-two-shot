import { useEffect, useState } from 'react'
import { useParams, useNavigate } from 'react-router-dom';

function ShoeDetails({ getShoes }) {
    const [ shoe, setShoe ] = useState({});
    const [ bin, setBin ] = useState([]);
    const { id } = useParams();
    const navigate = useNavigate();

    async function handleDeleteRequest(event) {
        event.preventDefault();

        const deleteUrl = `http://localhost:8080/api/shoes/${id}`
        const fetchOptions = {
            method: 'delete',
        };
        const deleteResponse = await fetch(deleteUrl, fetchOptions);
        if (deleteResponse.ok) {
            getShoes();
            navigate('/shoes/');
        }
    }

    async function getShoe() {
        const url = `http://localhost:8080/api/shoes/${id}`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setShoe(data);
            setBin(data.bin);
        }
    }

    useEffect(() => {
        getShoe();
    }, [])

    return (
        <>
            <img className='img-thumbnail my-3 mx-auto d-block w-50' src={shoe.picture_url} />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model Name</th>
                        <th>Color</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{shoe.manufacturer}</td>
                        <td>{shoe.model_name}</td>
                        <td>{shoe.color}</td>
                    </tr>
                </tbody>
            </table>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Bin</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Closet Name</td>
                        <td>{bin.closet_name}</td>
                    </tr>
                    <tr>
                        <td>Bin Number</td>
                        <td>{bin.bin_number}</td>
                    </tr>
                    <tr>
                        <td>Bin Size</td>
                        <td>{bin.bin_size}" x {bin.bin_size}"</td>
                    </tr>
                </tbody>
            </table>
            <button onClick={handleDeleteRequest} type='button' className='btn btn-danger my-2'>Delete Shoe</button>
        </>
    );
}

export default ShoeDetails;
