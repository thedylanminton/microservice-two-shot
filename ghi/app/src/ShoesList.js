import { Link, useNavigate } from 'react-router-dom'

function ShoesList({ shoes }) {
    const navigate = useNavigate();

    function handleAddShoe() {
        navigate('/shoes/new/');
    }

    return(
        <>
            <h1>Your Shoes</h1>
            <button onClick={handleAddShoe} className='btn btn-primary'>Add Shoe</button>
            <table className="table table-striped my-3">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model Name</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => {
                        return(
                            <tr key={shoe.id}>
                                <td>{ shoe.manufacturer }</td>
                                <td>{ shoe.model_name }</td>
                                <td><Link to={`${shoe.id}`}>Details</Link></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}

export default ShoesList;
